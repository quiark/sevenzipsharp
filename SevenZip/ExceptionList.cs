﻿using System;
using System.Collections.ObjectModel;

namespace SevenZip
{
    public class ExceptionList: Exception
    {
        private ReadOnlyCollection<Exception> exceptions;

        public ReadOnlyCollection<Exception> Exceptions
        {
            get { return exceptions; }
        }

        public ExceptionList(ReadOnlyCollection<Exception> exceptions)
        {
            this.exceptions = exceptions;
        }
    }
}